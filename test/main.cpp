/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */

#include "gtest/gtest.h"

namespace p44 {

TEST(Test, InitialTest)
{
    ASSERT_EQ(0, 0);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}

} // namespace p44

