/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */

#include "gmock/gmock.h"

namespace p44 {

class VdcMock : public Vdc
{
public:
  VdcMock(int aInstanceNumber, VdcHost *aVdcHostP, int aTag) : Vdc(aInstanceNumber, aVdcHostP, aTag) {}

  MOCK_METHOD2(scanForDevices, void(StatusCB, RescanMode));
  MOCK_CONST_METHOD0(vdcClassIdentifier, const char*());
  MOCK_CONST_METHOD0(vdcModelSuffix, string());
};

} //namespace p44

