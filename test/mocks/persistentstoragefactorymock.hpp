/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */

#include "gmock/gmock.h"
#include "ipersistentstoragefactory.hpp"

namespace p44 {


class PersistentStorageFactoryMock : public IPersistentStorageFactory
{
public:
  MOCK_METHOD1(addRecord, void(const PersistentRecord<std::string>& aRecord));
  MOCK_METHOD1(addRecord, void(const PersistentRecord<bool>& aRecord));
  MOCK_METHOD1(addRecord, void(const PersistentRecord<std::list<std::string>>& aRecord));

  virtual std::unique_ptr<IPersistentStorageWithRowId> createStorageWithRowId(const std::string& aRowId, const std::string& aTableName) override
  {
    return std::unique_ptr<IPersistentStorageWithRowId>(createStorageWithRowIdProxy(aRowId, aTableName));
  }

  MOCK_METHOD2(createStorageWithRowIdProxy, IPersistentStorageWithRowId*(const std::string& aRowId, const std::string& aTableName));
};

} //namespace p44

