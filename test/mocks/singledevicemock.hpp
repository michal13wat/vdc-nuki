/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */

#include "gmock/gmock.h"

namespace p44 {

class SingleDeviceMock : public SingleDevice
{
public:
  SingleDeviceMock(Vdc* aVdc) : SingleDevice(aVdc, true) {}

  MOCK_METHOD1(identifyDevice, bool(IdentifyDeviceCB aIdentifyCB));
};


} //namespace p44

