/*
 * nukidevice.hpp
 *
 *  Created on: Aug 10, 2018
 *      Author: vlad
 */
#pragma once
#include "p44vdc_common.hpp"

#include "simplescene.hpp"
#include "dsscene.hpp"
#include "singledevice.hpp"
#include "binaryinputbehaviour.hpp"
#include "buttonbehaviour.hpp"
#include "nukicomm.hpp"

namespace p44 {

namespace nuki {


  class DeviceSettings : public CmdSceneDeviceSettings
  {
    using inherited = CmdSceneDeviceSettings;

  public:

    explicit DeviceSettings(p44::Device &aDevice) :
      inherited(aDevice) {};

    virtual DsScenePtr newDefaultScene(SceneNo aSceneNo);
  };

  class Scene : public SimpleCmdScene
  {
    using inherited = SimpleCmdScene;

    void setAction(const string& aActionName);
  public:

    Scene(DeviceSettings &aSceneDeviceSettings, SceneNo aSceneNo) :
      inherited(aSceneDeviceSettings, aSceneNo)  {};

    virtual void setDefaultSceneValues(SceneNo aSceneNo);
  };
  using ScenePtr = boost::intrusive_ptr<Scene>;

  class Device : public SingleDevice
  {
    using inherited = SingleDevice;

  public:
    Device(p44::Vdc *aVdcP, const string& aIp, std::unique_ptr<IComm> aComm);

    virtual ~Device();

    virtual bool identifyDevice(IdentifyDeviceCB aIdentifyCB) P44_OVERRIDE;

    virtual string deviceTypeIdentifier() const P44_OVERRIDE { return "nuki"; };

    virtual string description() P44_OVERRIDE;

    virtual void initializeDevice(StatusCB aCompletedCB, bool aFactoryReset) P44_OVERRIDE;

    virtual string hardwareGUID() P44_OVERRIDE;

    virtual string hardwareModelGUID() P44_OVERRIDE;

    virtual string hardwareVersion() P44_OVERRIDE;

    virtual string modelName() P44_OVERRIDE;

    virtual string vendorName() P44_OVERRIDE;

    virtual string oemModelGUID() P44_OVERRIDE;

    virtual ErrorPtr handleMethod(VdcApiRequestPtr aRequest, const string &aMethod, ApiValuePtr aParams) P44_OVERRIDE;

  private:
    void configureDevice();
    void deriveDsUid();

    std::unique_ptr<IComm> comm;
  };

} // namespace nuki
} // namespace p44
