/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */


// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 6

#include "nukivdc.hpp"

#include "mainloop.hpp"
#include "makeintrusive.hpp"
#include "macaddress.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include "iconnectable.hpp"
#include "httpoperation.hpp"
#include "persistentstoragefactory.hpp"

using namespace p44::nuki;

namespace
{

class GetOperation : public p44::HttpOperation
{
  using inherited = p44::HttpOperation;
public:
  GetOperation(p44::HttpClient& aClient, const string& aIp, p44::HttpCommCB aCallback) :
    inherited(aClient, p44::HttpMethod::GET, "http://" + aIp + "/", {}, aCallback) {}

  virtual void sendRequest() P44_OVERRIDE
  {
    httpClient.getApi().clearRequestHeaders();
    issueRequest(nullptr, false, false);
  }
};

string macAddressFromHostName(const string& aHostName)
{
  if (aHostName.empty()) {
    return {};
  }

  static const string PREFIX = "bha-";
  static const size_t MAC_LENGTH = 12;

  if (!boost::starts_with(aHostName, PREFIX)) {
    LOG(LOG_DEBUG, "Cannot convert host name %s to mac address", aHostName.c_str());
    return {};
  }

  return aHostName.substr(PREFIX.size(), MAC_LENGTH);
}

string macAddressFromIp(const string& aIp)
{
  uint64_t macAddress;
  if (!p44::getMacAddressByIpv4(p44::stringToIpv4(aIp.c_str()), macAddress)) {
    LOG(LOG_DEBUG, "Cannot get device mac address (IP : %s)", aIp.c_str());
    return {};
  }
  return p44::macAddressToString(macAddress);
}

}


Vdc::Vdc(int aInstanceNumber, VdcHost *aVdcHostP, int aTag) :
  inherited(std::make_tuple(PersistentRecord<StringList>("hostIPs", hostIPs)),
            aInstanceNumber,
            aVdcHostP,
            aTag)
{
  httpClient.isMemberVariable();
  initializeName("Nuki Controller");
}

const char *Vdc::vdcClassIdentifier() const
{
  return "Nuki_Container";
}


bool Vdc::getDeviceIcon(string &aIcon, bool aWithData, const char *aResolutionPrefix)
{
  if (getIcon("vdc_nuki", aIcon, aWithData, aResolutionPrefix))
    return true;
  else
    return inherited::getDeviceIcon(aIcon, aWithData, aResolutionPrefix);
}


int Vdc::getRescanModes() const
{
  // normal and incremental make sense, no exhaustive mode
  return rescanmode_incremental+rescanmode_normal;
}

string Vdc::vdcModelSuffix() const
{
	return "";
}

void Vdc::scanForDevices(StatusCB aCompletedCB, RescanMode aRescanFlags)
{
	aCompletedCB(Error::ok());
}

