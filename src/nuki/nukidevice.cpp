/*
 * nukidevice.cpp
 *
 *  Created on: Aug 10, 2018
 *      Author: vlad
 */
// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 7

#include "makeintrusive.hpp"
#include "nukidevice.hpp"

using namespace p44::nuki;

p44::DsScenePtr DeviceSettings::newDefaultScene(SceneNo aSceneNo)
{
  ScenePtr scene = ScenePtr(new Scene(*this, aSceneNo));
  scene->setDefaultSceneValues(aSceneNo);
  return scene;
}

void Scene::setAction(const string& aActionName)
{
  value = 0;
  setDontCare(false);
  command = aActionName;
}

void Scene::setDefaultSceneValues(SceneNo aSceneNo)
{
  inherited::setDefaultSceneValues(aSceneNo);

  switch (aSceneNo) {
    case FIRE:
      setAction("std.ActDoorUnlock");
      break;
    default:
      setDontCare(true);
      break;
  }
  markClean();
}


Device::Device(p44::Vdc *aVdcP, const string& aIp, std::unique_ptr<IComm> aComm) :
  inherited(aVdcP),
  comm(std::move(aComm))
{

}

Device::~Device()
{

}

bool Device::identifyDevice(IdentifyDeviceCB aIdentifyCB)
{
  deriveDsUid();
  configureDevice();
  autoAddStandardActions();
  return true;
}

void Device::initializeDevice(StatusCB aCompletedCB, bool aFactoryReset)
{

}


void Device::configureDevice()
{

}


string Device::hardwareGUID()
{
  return "nukideviceid:";
}


string Device::hardwareModelGUID()
{
  return strcat("nukimodel:", "modelDesc");
}

string Device::hardwareVersion()
{
  return "fwVersion";
}

string Device::modelName()
{
  return "modelName";
}


string Device::vendorName()
{
  return "Nuki";
}

string Device::oemModelGUID()
{
  // temporary dummy number
  return "gs1:(01)1234567890123";
}

void Device::deriveDsUid()
{
  // vDC implementation specific UUID:
  DsUid vdcNamespace(DSUID_P44VDC_NAMESPACE_UUID);
  dSUID.setNameInSpace(hardwareGUID(), vdcNamespace);
}


string Device::description()
{
  return {};
}

p44::ErrorPtr Device::handleMethod(VdcApiRequestPtr aRequest, const string &aMethod, ApiValuePtr aParams)
{
  return SingleDevice::handleMethod(aRequest, aMethod, aParams);
}
