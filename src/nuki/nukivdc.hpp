/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */

#pragma once
#include "p44vdc_common.hpp"
#include "vdc.hpp"
#include "persistentstorage.hpp"
#include "httpclient.hpp"
#include "nukidevice.hpp"

namespace p44 {

namespace nuki {
  using StringList = std::list<string>;

  class Vdc : public PersistentStorage<p44::Vdc>
  {
    using inherited = PersistentStorage<p44::Vdc>;

    StringList hostIPs;
    HttpClient httpClient;

  public:

    Vdc(int aInstanceNumber, VdcHost *aVdcHostP, int aTag);

    virtual const char *vdcClassIdentifier() const P44_OVERRIDE;
    virtual int getRescanModes() const P44_OVERRIDE;
    virtual string vdcModelSuffix() const;
    bool getDeviceIcon(string &aIcon, bool aWithData, const char *aResolutionPrefix);
    virtual void scanForDevices(StatusCB aCompletedCB, RescanMode aRescanFlags);

  };


} // namespace nuki
} // namespace p44
