//
//  Copyright (c) 2017 digitalSTROM.org, Zurich, Switzerland
//
//  Author: Krystian Heberlein <krystian.heberlein@digitalstrom.com>
//
//  This file is part of vdc-netatmo.
//
//  dsdevices is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  dsdevices is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with p44vdc. If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "p44vdc_common.hpp"

#include "boost/optional.hpp"
#include "boost/signals2.hpp"
#include "jsonobject.hpp"
#include "jsonwebclient.hpp"
#include "serialqueue.hpp"
#include "httpoperation.hpp"
#include "httpclient.hpp"
#include "persistentstorage.hpp"
#include "statuscallback.hpp"


namespace p44 {

namespace nuki {


  using UpdateDataCB = boost::function<void(JsonObjectPtr)>;

  class IComm
  {
    public:
      virtual ~IComm() {}
      virtual boost::signals2::connection registerCallback(UpdateDataCB aCallback)=0;
  };

 class Comm : public IComm
 {
   public:

    using StatusUpdateCB = Callback<void(AccountStatus aStatus)>;

   private:
     MLTicket pollDataTicket;

     HttpClient httpClient;

     boost::signals2::signal<void(JsonObjectPtr)> dataPollCBs;
     PersistentStorageWithRowId<PersistentParams> storage;

     // basing on api description: 
     // "Do not try to pull data every minute. 
     // Netatmo Weather Station sends its measures to the server every ten minutes"
     static const MLMicroSeconds POLLING_INTERVAL = (10*Minute);
     static const int REFRESH_TOKEN_RETRY_MAX = 3;

     StatusUpdateCB statusUpdatedCB;

   public:

     Comm(ParamStore &aParamStore,  const string& aRowId, const string& aSslCAFile);
     virtual ~Comm();

     enum class Query {
         getStationsData,
         getHomeCoachsData,
         _num
     };

     void apiQuery(Query aQuery, HttpCommCB aResponseCB);

   private:
     void pollCycle();
     void cancelPollCycle();

     // TODO - change below IP address!!!!
     string bridgeIP = "127.0.0.1";

 };

 class Operation : public HttpOperation {

     using inherited = HttpOperation;

     static const MLMicroSeconds OP_TIMEOUT = (10*Second);

     const string& accessToken;
     const Comm::Query query;

   public:
     Operation(
         Comm::Query aQuery,
         HttpClient &aHttpClient,
         const string& aAccessToken,
         HttpCommCB aResultHandler,
         AuthCallback aAuthCallback={});
     virtual ~Operation(){}

     virtual OperationPtr finalize() P44_OVERRIDE;
 };
 using OperationPtr = boost::intrusive_ptr<Operation>;


} // namespace nunki

} // namespace p44

