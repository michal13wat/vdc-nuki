//
//  Copyright (c) 2017 digitalSTROM.org, Zurich, Switzerland
//
//  Author: Krystian Heberlein <krystian.heberlein@digitalstrom.com>
//
//  This file is part of vdc-netatmo.
//
//  dsdevices is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  dsdevices is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with dsdevices. If not, see <http://www.gnu.org/licenses/>.
//

// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 6

#include "nukicomm.hpp"

#include "sstream"
#include "boost/algorithm/string.hpp"


using namespace p44::nuki;


// MARK: ===== Operation


Operation::Operation(
    Comm::Query aQuery,
    HttpClient &aHttpClient,
    const string& aAccessToken,
    HttpCommCB aResultHandler,
    AuthCallback aAuthCallback
) :
    inherited(aHttpClient, HttpMethod::GET, {}, {}, aResultHandler, aAuthCallback),
    query(aQuery),
    accessToken(aAccessToken)
{
  this->setTimeout(OP_TIMEOUT);
}

p44::OperationPtr Operation::finalize()
{
  /*if chunked data has been read out, terminate http request*/
  httpClient.getApi().cancelRequest();
  return inherited::finalize();
}


Comm::Comm(ParamStore &aParamStore,  const string& aRowId, const string& aSslCAFile) :
    storage(
      aRowId,
      "CommSettings",
      make_tuple(
          PersistentRecord<string>("bridgeIP",   bridgeIP)
      ),
      aParamStore
    )
{
  httpClient.isMemberVariable();
  httpClient.getApi().setServerCertVfyDir(aSslCAFile);
  storage.load();
}


Comm::~Comm()
{
}


void Comm::apiQuery(Query aQuery, HttpCommCB aResponseCB)
{

}

void Comm::cancelPollCycle()
{
  pollDataTicket.cancel();
}


void Comm::pollCycle()
{
  pollDataTicket.executeOnce([&](auto...){ this->pollCycle(); }, POLLING_INTERVAL);
}


